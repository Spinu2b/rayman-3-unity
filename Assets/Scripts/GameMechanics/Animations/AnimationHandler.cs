﻿using Assets.Scripts.GameMechanics.Perso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Animations
{
    public class AnimationHandler : MonoBehaviour
    {
        PersoController perso;
        Dictionary<int, float> priorityCache = new Dictionary<int, float>();
        List<PersoAnimationState> nextAnimationStates = new List<PersoAnimationState>();
        public int currentAnimation => perso != null ? perso.animations.parameters.animationStateIndex : 0;

        private void Awake()
        {
            perso = gameObject.GetComponent<PersoController>();
        }

        public float currentPriority
        {
            get
            {
                if (priorityCache.ContainsKey(currentAnimation))
                {
                    return priorityCache[currentAnimation];
                } else
                {
                    return 0;
                }
            }
        }

        public void SetSpeed(float animationSpeed)
        {
            perso.animations.parameters.animationSpeed = animationSpeed;
        }

        public bool IsSet(float animationNumber)
        {
            return perso.animations.parameters.animationStateIndex == animationNumber;
        }

        public void Set(int animationNumber) => Set(animationNumber, currentPriority);
        public void Set(int animationNumber, float priority, float speed)
        {
            SetSpeed(speed);
            Set(animationNumber, priority);
        }

        public void Set(int animationNumber, float priority)
        {
            if (perso == null || animationNumber == currentAnimation || priority < currentPriority)
            {
                return;
            }

            foreach (var nextState in nextAnimationStates)
            {
                if (nextState.index == animationNumber)
                {
                    return;
                }
            }

            if (!priorityCache.ContainsKey(animationNumber))
            {
                priorityCache.Add(animationNumber, priority);
            }
            perso.animations.parameters.automaticNextAnimationState = true;
            perso.SetAnimationState(animationNumber);
            var nextAnimationState = perso.animations.currentAnimationState;

            nextAnimationStates.Clear();
            for (int i = 0; i < 1; i++)
            {
                if (nextAnimationState != null)
                {
                    nextAnimationStates.Add(nextAnimationState);
                }
            }
        }
    }
}
