﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Collision.Collide
{
    public class CollideComponent : MonoBehaviour
    {
        public GeometricObjectElementCollideTriangles collide;
        public CollideMaterial collideMaterial => collide?.gameMaterial?.collideMaterial;
    }
}
