﻿using Assets.Scripts.GameMechanics.Collision.Collide;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Collision
{
    public struct CollideInfo
    {
        public GeometricObjectElementCollideTriangles collide;
        public RaycastHit hit;
        public CollideMaterial material => collide.gameMaterial.collideMaterial;

        public bool isValid;

        public CollideType collideType => collide.objectCollide.type;
        public CollideMaterial.CollisionFlagsR3 type
        {
            get => (CollideMaterial.CollisionFlagsR3)material.type;
            set { material.type = (ushort)type; }
        }

        public CollideInfo(RaycastHit hit)
        {
            var collider = hit.collider?.GetComponent<CollideComponent>();
            if (collider != null && hit.point != Vector3.zero)
            {
                isValid = true;
                collide = collider.collide;
                this.hit = hit;
            }
            else if (MainStaticParameters.anyCollision)
            {
                isValid = true;
                collide = null;
                this.hit = hit;
            }
            else
            {
                isValid = false;
                collide = null;
                this.hit = new RaycastHit();
            }
        }

        public CollideInfo(RaycastHit hit, GeometricObjectElementCollideTriangles collide)
        {
            isValid = true;
            this.hit = hit;
            this.collide = collide;
        }

        bool Checks => MainStaticParameters.anyCollision || (isValid && collide?.gameMaterial?.collideMaterial != null);
        public bool None => !isValid;
        public bool Any => isValid;
        public bool Generic => isValid && collide?.gameMaterial?.collideMaterial == null;
        public bool AnyGround => Generic || GrabbableLedge || Trampoline;
        public bool AnyWall => Generic || GrabbableLedge || Slide || Trampoline || Wall || ClimbableWall;

        public bool Slide => Checks && (!MainStaticParameters.anyCollision && material.Slide);
        public bool Trampoline => Checks && (!MainStaticParameters.anyCollision && material.Trampoline);
        public bool GrabbableLedge => Checks && (!MainStaticParameters.anyCollision && material.GrabbableLedge);
        public bool Wall => Checks && (!MainStaticParameters.anyCollision && material.Wall);
        public bool FlagUnknown => Checks && (!MainStaticParameters.anyCollision && material.FlagUnknown);
        public bool HangableCeiling => Checks && (!MainStaticParameters.anyCollision && material.HangableCeiling);
        public bool ClimbableWall => Checks && (!MainStaticParameters.anyCollision && material.ClimbableWall);
        public bool Electric => Checks && (!MainStaticParameters.anyCollision && material.Electric);
        public bool LavaDeathWarp => Checks && (!MainStaticParameters.anyCollision && material.LavaDeathWarp);
        public bool FallTrigger => Checks && (!MainStaticParameters.anyCollision && material.FallTrigger);
        public bool HurtTrigger => Checks && (!MainStaticParameters.anyCollision && material.HurtTrigger);
        public bool DeathWarp => Checks && (!MainStaticParameters.anyCollision && material.DeathWarp);
        public bool FlagUnk2 => Checks && (!MainStaticParameters.anyCollision && material.FlagUnk2);
        public bool FlagUnk3 => Checks && (!MainStaticParameters.anyCollision && material.FlagUnk3);
        public bool Water => Checks && (!MainStaticParameters.anyCollision && material.Water);
        public bool NoCollision => Checks && (!MainStaticParameters.anyCollision && material.NoCollision);
    }
}
