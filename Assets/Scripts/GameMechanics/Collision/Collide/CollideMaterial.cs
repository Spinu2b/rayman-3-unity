﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.Collision.Collide
{
    public class CollideMaterial
    {
        public enum CollisionFlagsR3 : ushort
        {
            None = 0,
            Slide = 1 << 0,
            Trampoline = 1 << 1,
            GrabbaleLedge = 1 << 2,
            Wall = 1 << 3,
            FlagUnknown = 1 << 4,
            HangableCeiling = 1 << 5,
            ClimbableWall = 1 << 6,
            Electric = 1 << 7,
            LavaDeathWarp = 1 << 8,
            FallTrigger = 1 << 9,
            HurtTrigger = 1 << 10,
            DeathWarp = 1 << 11,
            FlagUnk2 = 1 << 12,
            FlagUnk3 = 1 << 13,
            Water = 1 << 14,
            NoCollision = 1 << 15,
            All = 0xFFFF
        }

        public ushort type;
        public ushort identifier;
        public CollisionFlagsR3 identifierR3;

        public bool Slide { get { return GetFlag(00); } set { SetFlag(00, value); } }
        public bool Trampoline { get { return GetFlag(01); } set { SetFlag(01, value); } }
        public bool GrabbableLedge { get { return GetFlag(02); } set { SetFlag(02, value); } }
        public bool Wall { get { return GetFlag(03); } set { SetFlag(03, value); } }
        public bool FlagUnknown { get { return GetFlag(04); } set { SetFlag(04, value); } }
        public bool HangableCeiling { get { return GetFlag(05); } set { SetFlag(05, value); } }
        public bool ClimbableWall { get { return GetFlag(06); } set { SetFlag(06, value); } }
        public bool Electric { get { return GetFlag(07); } set { SetFlag(07, value); } }
        public bool LavaDeathWarp { get { return GetFlag(08); } set { SetFlag(08, value); } }
        public bool FallTrigger { get { return GetFlag(09); } set { SetFlag(09, value); } }
        public bool HurtTrigger { get { return GetFlag(10); } set { SetFlag(10, value); } }
        public bool DeathWarp { get { return GetFlag(11); } set { SetFlag(11, value); } }
        public bool FlagUnk2 { get { return GetFlag(12); } set { SetFlag(12, value); } }
        public bool FlagUnk3 { get { return GetFlag(13); } set { SetFlag(13, value); } }
        public bool Water { get { return GetFlag(14); } set { SetFlag(14, value); } }
        public bool NoCollision { get { return GetFlag(15); } set { SetFlag(15, value); } }

        public void SetFlag(int index, bool value)
        {
            ushort flag = (ushort)(1 << index);
            if (value)
            {
                identifier = (ushort)(identifier | flag);
            }
            else
            {
                identifier = (ushort)(identifier & (ushort)(~flag));
            }
        }

        public bool GetFlag(int index)
        {
            ushort flag = (ushort)(1 << index);
            return (identifier & flag) != 0;
        }
        public bool GetFlag(CollisionFlagsR3 flags)
        {
            return (identifierR3 & flags) != CollisionFlagsR3.None;
        }
        public void SetFlag(CollisionFlagsR3 flags, bool on)
        {
            if (on)
            {
                identifierR3 = identifierR3 | flags;
            }
            else
            {
                identifierR3 = identifierR3 & (~flags);
            }
            identifier = (ushort)(identifierR3);
        }
    }
}
