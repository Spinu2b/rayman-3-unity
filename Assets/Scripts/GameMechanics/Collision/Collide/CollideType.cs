﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.Collision.Collide
{
    public enum CollideType
    {
        None = -1,
        ZDD = 0,
        ZDM = 1,
        ZDE = 2,
        ZDR = 3
    }
}
