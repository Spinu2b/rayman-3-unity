﻿using Assets.Scripts.GameMechanics.Collision.Collide;
using Assets.Scripts.GameMechanics.Collision.Materials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.Collision
{
    public class GeometricObjectElementCollideTriangles
    {
        public GeometricObjectCollide objectCollide;
        public GameMaterial gameMaterial;
    }
}
