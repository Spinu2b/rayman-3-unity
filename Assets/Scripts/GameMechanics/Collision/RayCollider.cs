﻿using Assets.Scripts.GameMechanics.Collision.Collide;
using Assets.Scripts.GameMechanics.Perso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Collision
{
    public class RayCollider
    {
        public bool groundEnabled = true;
        public bool wallEnabled;
        public float radius = 0.375f;
        public float bottom = 0.5f;
        public float top = 1;
        public float groundDepth = 0.25f;
        public float ceilingHeight = 0.75f;
        public float wallAngle = 0.707f;

        public static float waterShallowDepth = 1.5f;
        public bool waterAutoSurface = true;
        public float waterAutoSurfaceDepth = 4;
        public float waterRestOffset = 1.125f;

        public CollideInfo ground;
        public CollideInfo groundFar;
        public CollideInfo wall;
        public CollideInfo ceiling;
        public CollideInfo water;

        public bool atWaterSurface => water.hit.distance > 0 && water.hit.distance < 1.5f + waterRestOffset;
        public bool waterIsShallow => _waterIsShallow;
        bool _waterIsShallow;

        public MonoBehaviour controller;
        public PersoController perso => controller as PersoController;
        public PersoController platformPerso;
        public Vector3 position => perso != null ? perso.transformParameters.position : controller.transform.position;

        Vector3 wallPush;
        Vector3 ceilingPush;

        public CollideInfo RaycastGround(CollideMaterial.CollisionFlagsR3 types = CollideMaterial.CollisionFlagsR3.All) => Raycast(position + Vector3.up * 1, Vector3.down, 1 + groundDepth);
        public CollideInfo RaycastCeiling(CollideMaterial.CollisionFlagsR3 types = CollideMaterial.CollisionFlagsR3.All) => Raycast(position + Vector3.up * top, Vector3.up, ceilingHeight);

        public static CollideInfo Raycast(Vector3 origin, Vector3 direction, float distance, bool collideVisual = false)
        {
            bool hit = Physics.Raycast(origin, direction, out var newHit, distance, 1 << (collideVisual ? 8 : 9), QueryTriggerInteraction.Ignore);
            return new CollideInfo(newHit);
        }

        public void UpdateGroundCollision()
        {
            var tempPosition = position;
            if (groundEnabled)
            {
                var groundHold = RaycastGround();
                if (groundHold.hit.normal.y > wallAngle)
                {
                    ground = groundHold;
                } else
                {
                    ground = new CollideInfo();
                }

                platformPerso = ground.hit.collider?.GetComponentInParent<PersoController>();
                groundFar = Raycast(tempPosition + Vector3.up * 1, Vector3.down, 1 + 10);
            } else
            {
                ground = new CollideInfo();
                groundFar = new CollideInfo();
            }
        }

        public void StickToGround() => StickToGround(ref perso.transformParameters.position);
        public void StickToGround(ref Vector3 position)
        {
            if (!ground.AnyGround)
            {
                return;
            }
            if (platformPerso != null)
            {
                perso.transformParameters.position += platformPerso.transformParameters.deltaPosition;
            }
            position.y = ground.hit.point.y;
        }

        public void UpdateWallCollision()
        {
            if (!wallEnabled) return;
            wallPush = new Vector3();
            wall = new CollideInfo();
            var most = new Vector3();
            RaycastHit shortest = new RaycastHit { distance = radius };
            for (float height = bottom; height <= top; height += 0.25f)
            {
                for (float angle = 0; angle < Mathf.PI * 2; angle += Mathf.PI * 2 / 16)
                {
                    var collision = Raycast(position + height * Vector3.up, new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle)), radius);
                    if (collision.AnyWall && collision.hit.distance < shortest.distance)
                    {
                        most = collision.hit.normal * (-collision.hit.distance + radius);
                        shortest = collision.hit;
                        wall = collision;
                    }
                }
            }
            wallPush = new Vector3(most.x, most.y, most.z);
            ceilingPush = new Vector3();
            ceiling = RaycastCeiling();
            if (ceiling.AnyWall)
            {
                ceilingPush = ceiling.hit.normal * (-ceiling.hit.distance + ceilingHeight);
            }
        }

        public void ApplyWallCollision() => ApplyWallCollision(ref perso.transformParameters.position);
        public void ApplyWallCollision(ref Vector3 position)
        {
            position += wallEnabled ? wallPush : Vector3.zero + ceilingPush;
        }

        public void UpdateWaterCollision()
        {
            water = Raycast(position, Vector3.up, 4);
        }

        public void ApplyWaterCollision() => ApplyWaterCollision(ref perso.transformParameters.position, ref perso.physicsParams.velocityY);
        public void ApplyWaterCollision(ref Vector3 position)
        {
            throw new NotImplementedException();
        }

        public void ApplyWaterCollision(ref Vector3 position, ref float velocityY)
        {
            throw new NotImplementedException();
        }
    }
}
