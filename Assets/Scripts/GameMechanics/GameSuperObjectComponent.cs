﻿using Assets.Scripts.Utils.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics
{
    public class GameSuperObjectComponent : MonoBehaviour
    {
        private void Awake()
        {
            gameObject.AddComponent<InputEx>();
        }

        private void Start()
        {
            Screen.SetResolution(1920, 1080, true, 60);
        }
    }
}
