﻿using Assets.Scripts.GameMechanics.Perso;
using Assets.Scripts.GameMechanics.PersoControllers.Rayman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics
{
    public static class MainStaticParameters
    {
        public static bool anyCollision = true;
        public static bool useFixedTimeWithInterpolation = true;
        public static bool alwaysControlRayman = true;
        public static PersoController mainActor;
        public static RaymanPlayerController mainRayman;
    }
}
