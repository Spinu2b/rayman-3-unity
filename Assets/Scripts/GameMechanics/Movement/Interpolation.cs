﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Movement
{
    public interface IInterpolate
    {
        bool isFixedTimeInterpolation { get; }
        Vector3 interpolationPosition { get; }
        Quaternion interpolationRotation { get; }
    }

    public class Interpolation : MonoBehaviour
    {
        public static float deltaPositionThreshold = 8;

        Vector3 positionPrevious;
        Quaternion rotationPrevious;

        public MonoBehaviour fixedTimeController;

        IInterpolate interpolation;

        private void Update()
        {
            if (fixedTimeController == null) {
                return;
            }
            if (interpolation == null) {
                if (fixedTimeController is IInterpolate interpolateInterface) {
                    interpolation = interpolateInterface;
                }
                else {
                    return;
                }
            }

            if (interpolation.isFixedTimeInterpolation)
            {
                if ((interpolation.interpolationPosition - positionPrevious).magnitude < deltaPositionThreshold) {
                    transform.position = Vector3.Slerp(positionPrevious, interpolation.interpolationPosition, Time.deltaTime / Time.fixedDeltaTime);
                } else {
                    transform.position = interpolation.interpolationPosition;
                }
                positionPrevious = transform.position;

                transform.rotation = Quaternion.Slerp(rotationPrevious, interpolation.interpolationRotation, Time.deltaTime / Time.fixedDeltaTime);
                rotationPrevious = transform.rotation;
            }
            else
            {
                transform.position = interpolation.interpolationPosition;
                transform.rotation = interpolation.interpolationRotation;
            }
        }
    }
}
