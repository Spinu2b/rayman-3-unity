﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.Perso.Animations
{
    public class PersoAnimationParameters
    {
        public float animationSpeed;
        public int animationStateIndex;
        public bool automaticNextAnimationState;
    }
}
