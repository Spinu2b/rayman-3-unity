﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Perso.Animations
{
    public class PersoAnimationsComponent : MonoBehaviour
    {
        protected Animator animator;
        public PersoAnimationParameters parameters = new PersoAnimationParameters();
        public PersoAnimationState currentAnimationState
        {
            get {
                if (persoAnimationStates.ContainsKey(currentAnimationStateIndex))
                {
                    return persoAnimationStates[currentAnimationStateIndex];
                } else
                {
                    return null;
                }
                
            }
        }

        protected int currentAnimationStateIndex = 0;

        protected Dictionary<int, PersoAnimationState> persoAnimationStates = new Dictionary<int, PersoAnimationState>();

        private void Awake()
        {
            animator = gameObject.GetComponent<Animator>();   
        }

        public void SetAnimationState(int animationStateIndex)
        {
            if (animationStateIndex < 0 || !persoAnimationStates.ContainsKey(animationStateIndex))
            {
                //throw new InvalidOperationException("Passed animation state index which is not present in perso animation states! " + animationStateIndex);
                return;
            }
            currentAnimationStateIndex = animationStateIndex;
            PlayCurrentlySetAnimationClip();
        }

        public void BuildAnimationClipsInfo()
        {
            AssetDatabase.DeleteAsset("Assets/StateMachineTransitions.controller");
            var animatorController = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath("Assets/StateMachineTransitions.controller");

            var animationClips = Resources.LoadAll("Models/Rayman/rayman3texturedallclipss", typeof(AnimationClip)).Cast<AnimationClip>();
            animator.runtimeAnimatorController = animatorController;

            int clipIndex = 0;

            foreach (var animationClip in animationClips)
            {
                animationClip.wrapMode = WrapMode.Loop;
                var animatorState = animatorController.AddMotion(animationClip);
                persoAnimationStates.Add(clipIndex, new PersoAnimationState(clipIndex, animationClip.name));
                clipIndex++;
            }
        }

        protected void PlayCurrentlySetAnimationClip()
        {
            animator.speed = parameters.animationSpeed / 20.0f;
            animator.Play(currentAnimationState.animationClipName);
        }
    }
}
