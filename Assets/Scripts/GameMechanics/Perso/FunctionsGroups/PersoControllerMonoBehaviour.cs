﻿using Assets.Scripts.GameMechanics.Animations;
using Assets.Scripts.GameMechanics.Movement;
using Assets.Scripts.GameMechanics.Perso.Animations;
using Assets.Scripts.GameMechanics.PersoControllers.Rayman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.Perso
{
    public abstract partial class PersoController
    {
        protected void Awake()
        {
            visible = true;
            gameObject.AddComponent<Interpolation>().fixedTimeController = this;

            //if (this is RaymanPlayerController)
            //{
            if (gameObject.GetComponent<PersoAnimationsComponent>() == null)
            {
                animationHandler = gameObject.AddComponent<AnimationHandler>();
                animations = gameObject.AddComponent<PersoAnimationsComponent>();
            }            
            //}            

            rayCollision.controller = this;
            transformParameters.position = transform.position;
            transformParameters.rotation = transform.rotation;
            transformParameters.scale3 = transform.localScale;
            transformParameters.startPosition = transformParameters.position;
            transformParameters.startRotation = transformParameters.rotation;
        }

        protected void Start()
        {
            OnStart();
        }

        bool ActiveChecks() => timerDisable.active || outOfActiveRadius;

        protected void Update()
        {
            if (isMainActor || (MainStaticParameters.alwaysControlRayman && this is RaymanPlayerController))
            {
                if (MainStaticParameters.mainActor == null)
                {
                    MainStaticParameters.mainActor = this;
                    MainStaticParameters.mainRayman = (RaymanPlayerController)this;
                }

                OnInputMainActor();
            }

            if (!ActiveChecks())
            {
                OnInput();
            }
            if (!isFixedTimeInterpolation)
            {
                LogicLoop();
            }
        }

        protected void FixedUpdate()
        {
            if (isFixedTimeInterpolation)
            {
                LogicLoop();
            }
        }

        void InvokeRule(PersoControllerRule rule)
        {
            if (rule != null)
            {
                rule.Invoke(this, currentRuleArguments);
            }            
        }

        void LogicLoop()
        {
            if (ActiveChecks()) return;

            rayCollision.UpdateGroundCollision();
            rayCollision.UpdateWaterCollision();

            if (!timerDisable.active)
            {
                OnUpdate();
            }

            if (!timerDisable.active)
            {
                InvokeRule(currentRule);

                physicsParams.velocityY /= 1f + physicsParams.frictionY * globalFrictionMultiplier * deltaTime;
                physicsParams.velocityXZ /= 1f + physicsParams.frictionXZ * globalFrictionMultiplier * deltaTime;

                transformParameters.positionPrevious = transformParameters.position;
                transformParameters.position += new UnityEngine.Vector3(physicsParams.velocityXZ.x, physicsParams.velocityY, physicsParams.velocityXZ.z) * deltaTime;
                if (transformParameters.scale <= 0)
                {
                    transformParameters.scale = 0.0001f;
                }
                transform.localScale = transformParameters.scale3;
            }

            rayCollision.UpdateWallCollision();
            if (!timerDisable.active)
            {
                rayCollision.ApplyWallCollision();
            }

            transformParameters.deltaPosition = transformParameters.position - transformParameters.positionFrame;
            physicsParams.apprVelocity = transformParameters.deltaPosition / deltaTime;

            transformParameters.positionFrame = transformParameters.position;

            newRule = !AreSameRules(currentRule, previousRule);
            previousRule = currentRule;
        }
    }
}
