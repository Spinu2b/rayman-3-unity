﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Perso
{
    public abstract partial class PersoController
    {
        public void ApplyGravity() => physicsParams.velocityY = Mathf.Clamp(physicsParams.velocityY + physicsParams.gravity * globalGravityMultiplier * deltaTime, -80, 80);
        public void SetFriction(float horizontal, float vertical)
        {
            physicsParams.frictionXZ = horizontal;
            physicsParams.frictionY = vertical;
        }
    }
}
