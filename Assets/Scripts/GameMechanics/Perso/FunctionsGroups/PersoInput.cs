﻿using Assets.Scripts.Utils.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Perso
{
    public abstract partial class PersoController
    {
        public void InputMovement()
        {
            if ((isMainActor || MainStaticParameters.alwaysControlRayman) && InputEx.leftStick_s.magnitude > InputEx.deadZone)
            {
                float mults = physicsParams.frictionXZ * globalFrictionMultiplier * moveSpeed * deltaTime * -Mathf.Clamp(InputEx.leftStick_s.magnitude, -1, 1);
                physicsParams.velocityXZ += mults * new Vector3(
                    Mathf.Sin(transformParameters.rotation.eulerAngles.y * Mathf.Deg2Rad) * (1f + rayCollision.ground.hit.normal.x * 0.0f), 0,
                    Mathf.Cos(transformParameters.rotation.eulerAngles.y * Mathf.Deg2Rad) * (1f + rayCollision.ground.hit.normal.z * 0.0f));
            }
        }
    }
}
