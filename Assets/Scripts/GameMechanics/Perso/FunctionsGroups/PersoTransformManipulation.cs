﻿using Assets.Scripts.Utils.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Perso
{
    public abstract partial class PersoController
    {
        public void RotateToStick(float t = 10)
        {
            if ((isMainActor || MainStaticParameters.alwaysControlRayman) && InputEx.leftStick_s.magnitude < InputEx.deadZone)
            {
                return;
            }
            else
            {
                transformParameters.rotation = Quaternion.Lerp(transformParameters.rotation, Quaternion.Euler(0, InputEx.leftStickAngleCamera, 0),
                    t * Mathf.Clamp(InputEx.leftStick_s.sqrMagnitude, 0.2f, 50) * 2 * deltaTime);
            }
        }

        public void Orbit(Vector3 target, float angle, Vector3 offset, float tV = -1, float tH = -1)
        {
            var targetTemp = target + Matrix4x4.Rotate(Quaternion.Euler(0, angle - 180, 0)).MultiplyPoint3x4(offset);
            transformParameters.position.x = Mathf.Lerp(transformParameters.position.x, targetTemp.x, tCheck(tH));
            transformParameters.position.z = Mathf.Lerp(transformParameters.position.z, targetTemp.z, tCheck(tH));
            transformParameters.position.y = Mathf.Lerp(transformParameters.position.y, targetTemp.y, tCheck(tV));
        }

        public void LookAtY(Vector3 target, float addDegrees, float t = -1) 
            => transformParameters.rotation.eulerAngles = new Vector3(transformParameters.rotation.eulerAngles.x + addDegrees,
                lookAt(target, 0, addDegrees, t).eulerAngles.y, 0);
        public void LookAtX(Vector3 target, float addDegrees, float t = -1) 
            => transformParameters.rotation.eulerAngles = new Vector3(lookAt(target, addDegrees, 0, t).eulerAngles.x,
                transformParameters.rotation.eulerAngles.y, 0);

        protected float tCheck(float t) => t == -1 ? 1 : t * deltaTime;
        Quaternion lookAt(Vector3 target, float addDegreesX, float addDegreesY, float t) 
            => Quaternion.Slerp(transformParameters.rotation, 
                Matrix4x4.LookAt(transformParameters.position, target, Vector3.up).rotation * Quaternion.Euler(addDegreesX, addDegreesY - 180, 0), tCheck(t));
    }
}
