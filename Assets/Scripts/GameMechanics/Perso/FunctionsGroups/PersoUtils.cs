﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Perso
{
    public abstract partial class PersoController
    {
        public float DistToPerso(PersoController perso) => perso == null ? float.PositiveInfinity : DistTo(perso.transformParameters.position);
        public float DistTo(Vector3 point) => Vector3.Distance(transformParameters.position, point);

        public static T GetPerso<T>() where T : PersoController => FindObjectOfType<T>();
    }
}
