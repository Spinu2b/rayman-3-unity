﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Perso
{
    public class PersoAnimationState
    {
        public int index;
        public string animationClipName { get; private set; }

        public PersoAnimationState(int index, string animationClipName)
        {
            this.animationClipName = animationClipName;
            this.index = index;
        }
    }
}
