﻿using Assets.Scripts.GameMechanics.Animations;
using Assets.Scripts.GameMechanics.Collision;
using Assets.Scripts.GameMechanics.Movement;
using Assets.Scripts.GameMechanics.Perso.Animations;
using Assets.Scripts.Utils.Timer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Perso
{
    public abstract partial class PersoController : MonoBehaviour, IInterpolate
    {
        public RayCollider rayCollision = new RayCollider();
        public PersoPhysicsParams physicsParams = new PersoPhysicsParams();
        public PersoControllerRule currentRule;
        protected PersoControllerRule previousRule;
        protected List<object> currentRuleArguments = new List<object>();

        public Vector3 forward => Matrix4x4.Rotate(transformParameters.rotation).MultiplyVector(-Vector3.forward);

        public PersoTransformParameters transformParameters = new PersoTransformParameters();
        public PersoAnimationsComponent animations;

        public bool visible { get; private set; }

        public Timer timerDisable = new Timer();

        protected AnimationHandler animationHandler;

        public static float globalGravityMultiplier = 1.0f;
        public static float globalFrictionMultiplier = 1.0f;
        public virtual float activeRadius => 75;

        public PersoController mainActor => MainStaticParameters.mainActor;
        public bool isMainActor => this == mainActor;
        public bool outOfActiveRadius => MainStaticParameters.mainActor == null || DistToPerso(mainActor) > activeRadius;

        public bool newRule { get; private set; }

        public float moveSpeed = 10.0f;

        public virtual bool isFixedTimeInterpolation => MainStaticParameters.useFixedTimeWithInterpolation;
        public Vector3 interpolationPosition => transformParameters.position;
        public Quaternion interpolationRotation => transformParameters.rotation;
        protected float deltaTime => isFixedTimeInterpolation ? Time.fixedDeltaTime : Time.deltaTime;

        public void SetRule(PersoControllerRule rule)
        {
            currentRule = rule;
        }

        public void SetAnimationState(int animationStateIndex)
        {
            animations.SetAnimationState(animationStateIndex);
        }

        public bool IsCurrentRule(PersoControllerRule rule)
        {
            return currentRule != null && currentRule.name.Equals(rule.name);
        }

        public bool IsCurrentRule(string ruleName)
        {
            return currentRule != null && currentRule.name.Equals(ruleName);
        }

        public bool AreSameRules(PersoControllerRule ruleA, PersoControllerRule ruleB)
        {
            return (ruleA == null && ruleB == null) || (ruleA != null && ruleB != null && ruleA.name.Equals(ruleB.name));
        }

        protected virtual void OnStart() { }
        protected virtual void OnInput() { }
        protected virtual void OnInputMainActor() { }
        protected virtual void OnUpdate() { }
        protected virtual void OnDeath() { }
    }
}
