﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.Perso
{
    public delegate void PersoRuleDelegate(PersoController controller, List<object> ruleArguments);

    public class PersoControllerRule
    {
        public string name;
        protected PersoRuleDelegate ruleAction;

        public PersoControllerRule(string name, PersoRuleDelegate ruleAction)
        {
            this.name = name;
            this.ruleAction = ruleAction;
        }

        public void Invoke(PersoController controller, List<object> ruleArguments)
        {
            ruleAction.Invoke(controller, ruleArguments);
        }
    }

    public class PersoControllerRuleFactory
    {
        public static PersoControllerRule NewRule(string ruleName, PersoRuleDelegate action)
        {
            return new PersoControllerRule(name: ruleName, ruleAction: action);
        }
    }
}
