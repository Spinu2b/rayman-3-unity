﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Perso
{
    public class PersoPhysicsParams
    {
        public float velocityY;
        public Vector3 velocityXZ;
        public Vector3 apprVelocity; 
        public float jumpLiftingOffVelocityY;
        public float liftingOffVelocity;
        public float jumpCutoff;
        public float gravity = -25;
        public float frictionXZ;
        public float frictionY;
    }
}
