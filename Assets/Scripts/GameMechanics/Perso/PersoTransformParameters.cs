﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Perso
{
    public class PersoTransformParameters
    {
        public Quaternion rotation;
        public Vector3 position;
        public float scale;
        public Vector3 scale3;
        public Vector3 startPosition;
        public Quaternion startRotation;
        public Vector3 deltaPosition;
        public Vector3 positionPrevious;
        public Vector3 positionFrame;
    }
}
