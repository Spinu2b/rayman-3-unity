﻿using Assets.Scripts.GameMechanics.Perso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.PersoControllers.Camera
{
    public abstract class CameraController : PersoController
    {
        public UnityEngine.Camera camera => UnityEngine.Camera.main;
        public override bool isFixedTimeInterpolation => true;
        public override float activeRadius => 100000;
    }
}
