﻿using Assets.Scripts.GameMechanics.Perso;
using Assets.Scripts.Utils.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.PersoControllers.Camera.StandardCamera
{
    public partial class StandardCamera
    {
        public void RuleFollow()
        { 
            orbitRotation -= (InputEx.rightStick.x * 135) * deltaTime;
            /* */
            if (target.IsCurrentRule(PersoRulesNames.Air))
            {
                timerLandOrbitSpeed.Abort();
                orbitSpeed = 0.08f;
            }
            else if (target.newRule)
            {
                timerLandOrbitSpeed.Start(0.3f, () => orbitSpeed = defaultOrbitSpeed, false);
            }

            if (InputEx.leftStickPress && InputEx.leftStickAngle > -160 && InputEx.leftStickAngle < 160)
            {
                orbitVelocity = Mathf.Lerp(orbitVelocity, Mathf.Clamp(-InputEx.leftStickAngle * 
                    new Vector3(target.physicsParams.apprVelocity.x, 0, target.physicsParams.apprVelocity.z).magnitude
                    * orbitSpeed, -120, 120), 15 * deltaTime);
            } else
            {
                orbitVelocity = Mathf.Lerp(orbitVelocity, 0, 20 * deltaTime);
            }

            orbitRotation += orbitVelocity * deltaTime;

            if (!targetIsRayman)
            {
                tY = 20;
                SetOrbitOffset(defaultCameraOffset);
            }
            else
            {
                if (MainStaticParameters.mainRayman.movementStateFlags.helicopter)
                {
                    SetOrbitOffset(defaultCameraOffset + new Vector3(0, 2.75f, 1.25f), 2);
                } else if (target.IsCurrentRule(PersoRulesNames.Ground))
                {
                    tY = 9;
                    if (target.physicsParams.velocityXZ.magnitude < target.moveSpeed / 2) {
                        SetOrbitOffset(defaultCameraOffset, 2);
                    }
                    else {
                        SetOrbitOffset(defaultCameraOffset + new Vector3(0, 0.7f * -target.physicsParams.apprVelocity.y, 0.6f), 3);
                    }
                } else if (target.IsCurrentRule(PersoRulesNames.Air)) {
                    if (MainStaticParameters.mainRayman.movementStateFlags.jumping && target.physicsParams.velocityY > 0) {
                        tY = 1.5f;
                        SetOrbitOffset(defaultCameraOffset + new Vector3(0, -1.5f, -1), 2);
                    }
                    else if (target.rayCollision.groundFar.hit.distance > 4) {
                        timerTY.Start(0.3f, () => tY = 3);
                    }
                }

                if (target.physicsParams.velocityY > 0)
                {
                    xLook = Mathf.Lerp(xLook, -2, 8 * deltaTime);
                } else
                {
                    xLook = Mathf.Lerp(xLook, 8, 8 * deltaTime);
                }

                if (target.rayCollision.groundFar.AnyGround && target.rayCollision.groundFar.hit.distance < 2.5f)
                {
                    center = target.rayCollision.groundFar.hit.point;
                } else
                {
                    center = target.transformParameters.position;
                }

                Orbit(center, orbitRotation, offset, tY, 8);
                LookAtY(target.transformParameters.position, 0);
                LookAtX(target.transformParameters.position, xLook, 4);
               
            }
            /* */
        }
    }
}
