﻿using Assets.Scripts.GameMechanics.Perso;
using Assets.Scripts.GameMechanics.PersoControllers.Rayman;
using Assets.Scripts.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.PersoControllers.Camera.StandardCamera
{
    public partial class StandardCamera : CameraController
    {
        public bool targetIsRayman => target is RaymanPlayerController;
        public PersoController target => mainActor;

        public static Vector3 defaultCameraOffset = new Vector3(0, 4, 6.5f);
        public static float defaultOrbitSpeed = 0.135f;

        float orbitRotation;
        float orbitSpeed;
        float orbitVelocity = defaultOrbitSpeed;

        Vector3 focus;
        Vector3 offset = defaultCameraOffset;

        Timer timerLandOrbitSpeed = new Timer();
        Timer timerTY = new Timer();
        float xLook;

        float tY { get => _tY; set { _tY = Mathf.Lerp(_tY, value, deltaTime * 5); } }
        float _tY;

        Vector3 center;

        public void SetOrbitRotation(float degrees, float t = -1) => orbitRotation = Quaternion.Slerp(Quaternion.Euler(0, orbitRotation, 0), Quaternion.Euler(0, degrees, 0), tCheck(t)).eulerAngles.y;
        public void SetOrbitOffset(Vector3 offset, float t = -1) => this.offset = Vector3.Lerp(this.offset, offset, tCheck(t));

        protected override void OnStart()
        {
            rayCollision.wallEnabled = true;
            SetRule(StandardCameraRules.RuleFollowObj);
        }

        protected override void OnUpdate()
        {
            LevelRules();
            camera.transform.position = transformParameters.position;

            camera.transform.rotation = transformParameters.rotation;
            camera.transform.LookAt(transformParameters.position + forward, Vector3.up);
        }
    }
}
