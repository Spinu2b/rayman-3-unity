﻿using Assets.Scripts.GameMechanics.Perso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.PersoControllers.Camera.StandardCamera
{
    public static class StandardCameraRules
    {
        public static PersoControllerRule RuleFollowObj = PersoControllerRuleFactory.NewRule("Follow", (controller, ruleArguments) => ((StandardCamera)controller).RuleFollow());
    }
}
