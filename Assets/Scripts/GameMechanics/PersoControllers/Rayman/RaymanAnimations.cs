﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.PersoControllers.Rayman
{
    public static class RaymanAnimations
    {
        // TODO: Assign correct animation numbers in here
        public const int None = -1;
        public const int Idle = 0;

        public const int Tiptope = 177;
        public const int Walk = 1;
        
        public const int Jog = 178;
        public const int Run = 2;
        public const int RunStart = 179;
        public const int RunStop = 144;

        public const int LandIdle = 0;
        public const int LandWalk = 93;

        public const int LandRun = 282;

        public const int JumpIdleStart = 4;
        public const int FallIdleStart = 6;

        public const int JumpIdleLoop = 7;
        public const int FallIdleLoop = 8;

        public const int JumpRunStart = 89;
        public const int FallRunStart = 90;

        public const int JumpRunLoop = 91;
        public const int FallRunLoop = 92;

        public const int Despawn = 161;
        public const int Respawn = 162;

        public const int HelicopterEnable = 17;
        public const int HelicopterIdle = 16;
        public const int HelicopterDisable = 16;
        public const int HelicopterLandingIdle = 1;
        public const int HelicopterLandingWalk = 1;

        public const int ClimbWallStart = 1;
        public const int ClimbWallUpStart = 1;
        public const int ClimbWallRightStart = 1;
        public const int ClimbWallLeftStart = 1;
        public const int ClimbWallDownStart = 1;
        public const int ClimbWallUpEnd = 1;
        public const int ClimbWallRightEnd = 1;
        public const int ClimbWallDownEnd = 1;
        public const int ClimbWallLeftEnd = 1;

        public const int DeathFall = 1;

        public const int SlideToRun = 1;

        public const int SwimEnter = 1;
        public const int SwimStartMove = 1;
        public const int SwimStopMove = 1;
    }
}
