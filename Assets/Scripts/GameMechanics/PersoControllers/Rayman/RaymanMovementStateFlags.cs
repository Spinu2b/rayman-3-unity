﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.PersoControllers.Rayman
{
    public class RaymanMovementStateFlags
    {
        public bool jumping;
        public bool strafing;
        public bool helicopter;
        public bool selfJump;
        public bool slideJump;
    }
}
