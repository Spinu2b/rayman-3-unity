﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.GameMechanics.Animations;
using Assets.Scripts.GameMechanics.Collision;
using Assets.Scripts.GameMechanics.Perso;
using Assets.Scripts.GameMechanics.Perso.Animations;
using Assets.Scripts.GameMechanics.PersoControllers.Camera.StandardCamera;
using Assets.Scripts.GameMechanics.PersoControllers.Rayman;
using Assets.Scripts.Utils.Input;
using Assets.Scripts.Utils.Timer;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.PersoControllers.Rayman
{
    public partial class RaymanPlayerController : PersoController
    {
        public RaymanMovementStateFlags movementStateFlags = new RaymanMovementStateFlags();

        public StandardCamera camera;

        public float trampolineJumpHeight = 16.0f;
        public float jumpHeight = 4.0f;
        public float climbingVelocityYThreshold = 2.0f;
        public float groundDepth = 0.5f;

        protected float superHelicopterRev;

        protected CollideInfo collisionClimb;

        protected Timer timerRunStart = new Timer();
        protected Timer timerRespawn = new Timer();

        protected override void OnStart()
        {
            camera = GetPerso<StandardCamera>();
            animations = GetComponent<PersoAnimationsComponent>();
            animationHandler = GetComponent<AnimationHandler>();
            animations.BuildAnimationClipsInfo();
            SetRule(RaymanPlayerControllerRules.RuleAirObj);
        }

        protected override void OnUpdate()
        {
            if (rayCollision.ground.FallTrigger)
            {
                SetRule(RaymanPlayerControllerRules.RuleFallingObj);
            }
            else if (rayCollision.ground.Trampoline)
            {
                Jump(trampolineJumpHeight, true);
            }
            else if (rayCollision.wall.ClimbableWall && physicsParams.velocityY <= climbingVelocityYThreshold)
            {
                SetRule(RaymanPlayerControllerRules.RuleClimbingObj);
            }
        }

        protected override void OnInputMainActor()
        {
            if (IsCurrentRule(RaymanPlayerControllerRules.RuleGroundObj))
            { 
                if (InputEx.iJumpDown)
                {
                    Jump(height: jumpHeight, forceMaxHeight: false, selfJump: true);
                }
            }
            else if (IsCurrentRule(RaymanPlayerControllerRules.RuleClimbingObj))
            {
                if (InputEx.iJumpDown)
                {
                    Jump(height: jumpHeight, forceMaxHeight: false, selfJump: true);
                }
            } 
            else if (IsCurrentRule(RaymanPlayerControllerRules.RuleAirObj))
            {
                if (movementStateFlags.jumping && UnityEngine.Input.GetButtonUp(InputButtons.Jump))
                {
                    movementStateFlags.jumping = false;
                }

                if (InputEx.iJumpDown && !movementStateFlags.helicopter)
                {
                    ToggleHelicopter();
                }
            }
        }

        protected void Jump(float height, bool forceMaxHeight, bool selfJump = false, bool slideJump = false)
        {
            movementStateFlags.selfJump = selfJump;
            movementStateFlags.jumping = true;
            movementStateFlags.helicopter = false;
            currentRule = RaymanPlayerControllerRules.RuleAirObj;

            float am = Mathf.Sqrt((1920f / 97) * height);
            physicsParams.jumpLiftingOffVelocityY = slideJump ? physicsParams.apprVelocity.y / 2 : 0;
            physicsParams.jumpCutoff = am * 0.65f + physicsParams.jumpLiftingOffVelocityY;
            physicsParams.velocityY = am * 1.25f + physicsParams.jumpLiftingOffVelocityY;

            if (physicsParams.velocityXZ.magnitude < moveSpeed / 2 || !selfJump)
            {
                animationHandler.Set(animationNumber: RaymanAnimations.JumpIdleStart, priority: 1);
            }               
            else
            {
                animationHandler.Set(animationNumber: RaymanAnimations.JumpRunStart, priority: 1);
            }
        }

        protected void ToggleHelicopter()
        {
            movementStateFlags.helicopter = !movementStateFlags.helicopter;
            if (!movementStateFlags.helicopter)
            {
                animationHandler.Set(animationNumber: RaymanAnimations.HelicopterDisable, priority: 1.0f);
            }
        }

        public void RespawnRayman()
        {
            throw new NotImplementedException();
        }
    }
}
