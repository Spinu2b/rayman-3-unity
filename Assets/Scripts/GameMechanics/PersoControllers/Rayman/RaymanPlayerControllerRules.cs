﻿using Assets.Scripts.GameMechanics.Perso;
using Assets.Scripts.Utils.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.PersoControllers.Rayman
{
    public static class RaymanPlayerControllerRules
    {
        public static PersoControllerRule RuleAirObj = 
            PersoControllerRuleFactory.NewRule("Air", (controller, ruleArguments) => ((RaymanPlayerController)controller).RuleAir());

        public static PersoControllerRule RuleFallingObj =
            PersoControllerRuleFactory.NewRule("Falling", (controller, ruleArguments) => ((RaymanPlayerController)controller).RuleFalling());

        public static PersoControllerRule RuleClimbingObj =
            PersoControllerRuleFactory.NewRule("Climbing", (controller, ruleArguments) => ((RaymanPlayerController)controller).RuleClimbing());

        public static PersoControllerRule RuleGroundObj =
            PersoControllerRuleFactory.NewRule("Ground", (controller, ruleArguments) => ((RaymanPlayerController)controller).RuleGround());

        public static PersoControllerRule RuleSwimmingObj =
            PersoControllerRuleFactory.NewRule("Swimming", (controller, ruleArguments) => ((RaymanPlayerController)controller).RuleSwimming());
    }
}
