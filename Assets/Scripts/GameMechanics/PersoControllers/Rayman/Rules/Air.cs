﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.PersoControllers.Rayman
{
    public partial class RaymanPlayerController
    {
        public void RuleAir()
        {
            #region Rule
            rayCollision.groundDepth = 0;
            rayCollision.UpdateGroundCollision();

            if (newRule) {
                physicsParams.liftingOffVelocity = physicsParams.velocityXZ.magnitude;
            }

            if (rayCollision.ground.AnyGround && physicsParams.velocityY <= 0) {
                physicsParams.velocityY = 0;
                SetRule(RaymanPlayerControllerRules.RuleGroundObj);
                return;
            } else if (rayCollision.ground.Slide) {
                //SetRule(RaymanPlayerControllerRules.RuleSlidingObj);
                return;
            } else if (rayCollision.ground.Water && physicsParams.velocityY < 0 && !rayCollision.waterIsShallow) {
                SetRule(RaymanPlayerControllerRules.RuleSwimmingObj);
                return;
            }

            if (movementStateFlags.jumping) {
                physicsParams.gravity = -13;
                if (physicsParams.velocityY < physicsParams.jumpCutoff) {
                    movementStateFlags.jumping = false;
                }
            } else {
                physicsParams.gravity = -27;
            }

            ApplyGravity();

            if (movementStateFlags.helicopter) {
                superHelicopterRev = Mathf.Lerp(superHelicopterRev, 0, deltaTime * 1.0f);

                SetFriction(10, 7.5f);
                moveSpeed = 5;
                physicsParams.velocityY += deltaTime * superHelicopterRev;
                physicsParams.velocityY = Mathf.Clamp(physicsParams.velocityY, -5, 5);
                movementStateFlags.selfJump = false;
            } else {
                if (movementStateFlags.slideJump) {
                    SetFriction(0.1f, 0);
                } else {
                    SetFriction(5.0f, 0);
                }
                moveSpeed = 10f;
            }

            RotateToStick(6);
            InputMovement();

            if (transformParameters.position.y < transformParameters.startPosition.y - 1100) {
                SetRule(RaymanPlayerControllerRules.RuleFallingObj);
            }
            #endregion
            #region Animation

            if (movementStateFlags.helicopter) {
                animationHandler.Set(animationNumber: RaymanAnimations.HelicopterEnable, priority: 1.0f);
            } else if (physicsParams.liftingOffVelocity < 5 || !movementStateFlags.selfJump) {
                if (physicsParams.velocityY > 5 + physicsParams.jumpLiftingOffVelocityY) {
                    animationHandler.Set(animationNumber: RaymanAnimations.JumpIdleLoop, priority: 0);
                } else {
                    if (newRule) {
                        animationHandler.Set(animationNumber: RaymanAnimations.FallIdleLoop, priority: 0.0f);
                    } else {
                        animationHandler.Set(animationNumber: RaymanAnimations.FallIdleStart, priority: 1.0f);
                    }
                }
            } else {
                if (physicsParams.velocityY > 5 + physicsParams.jumpLiftingOffVelocityY) {
                    animationHandler.Set(animationNumber: RaymanAnimations.JumpRunLoop, priority: 0.0f);
                } else {
                    if (newRule) {
                        animationHandler.Set(animationNumber: RaymanAnimations.FallRunLoop, priority: 0.0f);
                    } else {
                        animationHandler.Set(animationNumber: RaymanAnimations.FallRunStart, priority: 1.0f);
                    }
                }
            }
            #endregion
        }
    }
}
