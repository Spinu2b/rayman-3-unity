﻿using Assets.Scripts.GameMechanics.Collision;
using Assets.Scripts.Utils.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.PersoControllers.Rayman
{
    public partial class RaymanPlayerController
    {
        public void RuleClimbing() {
            #region Rule
            if (newRule) {
                physicsParams.velocityXZ = Vector3.zero;
                physicsParams.velocityY = 0;
                animationHandler.Set(animationNumber: RaymanAnimations.ClimbWallStart, priority: 1.0f);
  
                if (rayCollision.wall.hit.point != Vector3.zero) {
                    transformParameters.position = rayCollision.wall.hit.point + rayCollision.wall.hit.normal * 0.5f;
                }
                collisionClimb = rayCollision.wall;
            }

            if ((collisionClimb = RayCollider.Raycast(transformParameters.position + collisionClimb.hit.normal, -collisionClimb.hit.normal, 3)).ClimbableWall)
            {
                transformParameters.rotation = Matrix4x4.LookAt(transformParameters.position, transformParameters.position + collisionClimb.hit.normal, Vector3.up).rotation;
                transformParameters.position = collisionClimb.hit.point + collisionClimb.hit.normal * 0.5f;
                if (InputEx.leftStick.magnitude > InputEx.deadZone)
                {
                    transformParameters.position += Matrix4x4.Rotate(transformParameters.rotation).MultiplyVector(new Vector2(-InputEx.leftStick_s.x, InputEx.leftStick_s.y)) * 6 * deltaTime;
                }
            } else if (physicsParams.apprVelocity.y > 2 && InputEx.leftStickAngle * Mathf.Sign(InputEx.leftStickAngle) < 30) {
                Jump(height: 4, forceMaxHeight: false);
            }

            rayCollision.wallEnabled = false;
            #endregion
            #region Animation
            float leftStickAngle = 0;
            if (InputEx.leftStick_s.magnitude > InputEx.deadZone)
            {
                leftStickAngle = InputEx.leftStickAngle;
                animationHandler.SetSpeed(InputEx.leftStick_s.magnitude * 35);
                if (leftStickAngle > -45 && leftStickAngle < 45) {
                    animationHandler.Set(animationNumber: RaymanAnimations.ClimbWallUpStart, priority: 1.0f);
                } else if (leftStickAngle >= 45 && leftStickAngle <= 135) {
                    animationHandler.Set(animationNumber: RaymanAnimations.ClimbWallRightStart, priority: 1.0f);
                } else if (leftStickAngle > 135 || leftStickAngle < -135) {
                    animationHandler.Set(animationNumber: RaymanAnimations.ClimbWallDownStart, priority: 1.0f);
                } else if (leftStickAngle >= -135 && leftStickAngle <= -45) {
                    animationHandler.Set(animationNumber: RaymanAnimations.ClimbWallLeftStart, priority: 1.0f);
                }
            } else {
                animationHandler.SetSpeed(25);
                if (leftStickAngle > -45 && leftStickAngle < 45) {
                    animationHandler.Set(animationNumber: RaymanAnimations.ClimbWallUpEnd, priority: 1.0f);
                } else if (leftStickAngle > 45 && leftStickAngle < 135) {
                    animationHandler.Set(animationNumber: RaymanAnimations.ClimbWallRightEnd, priority: 1.0f);
                } else if (leftStickAngle > 135 || leftStickAngle < -135) {
                    animationHandler.Set(animationNumber: RaymanAnimations.ClimbWallDownEnd, priority: 1.0f);
                } else if (leftStickAngle > -135 && leftStickAngle < -45) {
                    animationHandler.Set(animationNumber: RaymanAnimations.ClimbWallLeftEnd, priority: 1.0f);
                }
            }
            #endregion
        }
    }
}
