﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.PersoControllers.Rayman
{
    public partial class RaymanPlayerController
    {
        public void RuleFalling()
        {
            if (newRule) {
                transformParameters.scale = 1;
            }
            if (transformParameters.scale <= 0) {
                return;
            }
            transformParameters.scale -= deltaTime / 2.5f;
            if (transformParameters.scale <= 0) {
                timerRespawn.Start(0.1f, RespawnRayman);
            }

            SetFriction(1, 2);
            ApplyGravity();

            animationHandler.Set(animationNumber: RaymanAnimations.DeathFall, priority: 1.0f);
        }
    }
}
