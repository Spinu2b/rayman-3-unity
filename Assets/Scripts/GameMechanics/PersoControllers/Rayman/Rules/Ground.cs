﻿using Assets.Scripts.Utils.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.PersoControllers.Rayman
{
    public partial class RaymanPlayerController
    {
        public void RuleGround()
        {
            #region Rule
            rayCollision.groundDepth = groundDepth;
            rayCollision.UpdateGroundCollision();

            if (newRule && InputEx.leftStick.magnitude < InputEx.deadZone) {
                physicsParams.velocityXZ = Vector3.zero;
            }

            movementStateFlags.selfJump = false;
            movementStateFlags.slideJump = false;

            if (rayCollision.ground.AnyGround && rayCollision.ground.hit.distance < 1.5f) {
                rayCollision.StickToGround();
            }
            else if (rayCollision.ground.Slide) {
                //SetRule(RaymanPlayerControllerRules.Sliding); return;
            }
            else if (rayCollision.water.Water && !rayCollision.waterIsShallow) {
                SetRule(RaymanPlayerControllerRules.RuleSwimmingObj); return;
            }
            else {
                SetRule(RaymanPlayerControllerRules.RuleAirObj); return;
            }

            SetFriction(30, 0);

            if (movementStateFlags.strafing) {
                moveSpeed = 7.0f;
            }
            else {
                moveSpeed = 10.0f;
            }

            InputMovement();
            RotateToStick(10);

            transformParameters.rotation = Quaternion.Slerp(transformParameters.rotation, Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0), deltaTime * 10);
            #endregion
            #region Animation

            if (physicsParams.velocityXZ.magnitude < 9f) {
                timerRunStart.Start(0.033f);
                if (newRule) {
                    if (movementStateFlags.helicopter) {
                        animationHandler.Set(animationNumber: RaymanAnimations.HelicopterLandingIdle, priority: 1.0f);
                    }
                    else {
                        animationHandler.Set(animationNumber: RaymanAnimations.LandIdle, priority: 1.0f);
                    }
                }
                else {
                    animationHandler.Set(animationNumber: RaymanAnimations.Idle, priority: 0.0f);
                }
                if (animationHandler.currentAnimation == RaymanAnimations.RunStop) {
                    animationHandler.SetSpeed(40.0f);
                }
                else {
                    animationHandler.SetSpeed(25.0f);
                }
            }
            else if (physicsParams.velocityXZ.magnitude < 9.0f)
            {
                if (newRule) {
                    if (movementStateFlags.helicopter) {
                        animationHandler.Set(animationNumber: RaymanAnimations.HelicopterLandingWalk, priority: 1.0f);
                    } else {
                        animationHandler.Set(animationNumber: RaymanAnimations.LandWalk, priority: 1.0f);
                    }
                }
                else {
                    animationHandler.Set(animationNumber: RaymanAnimations.Idle, priority: 0.0f);
                }
                float speed = physicsParams.velocityXZ.magnitude * moveSpeed * 1.5f;

                if (animationHandler.currentAnimation == RaymanAnimations.HelicopterLandingWalk) {
                    animationHandler.SetSpeed(speed / 2.0f);
                }
                else {
                    animationHandler.SetSpeed(speed);
                }
            }
            else {
                if (newRule) {
                    if (movementStateFlags.helicopter) {
                        animationHandler.Set(animationNumber: RaymanAnimations.HelicopterLandingWalk, priority: 1.0f);
                    } else {
                        animationHandler.Set(animationNumber: RaymanAnimations.LandRun, priority: 1.0f);
                    }
                } else {
                    if (animationHandler.currentAnimation == RaymanAnimations.RunStart) {
                        animationHandler.SetSpeed(200);
                    } else if (animationHandler.IsSet(RaymanAnimations.SlideToRun)) {
                        animationHandler.SetSpeed(30);
                    } else {
                        animationHandler.Set(RaymanAnimations.Run, 0.0f, physicsParams.velocityXZ.magnitude * moveSpeed * 0.46f);
                    }
                }

                if (animationHandler.currentAnimation == RaymanAnimations.HelicopterLandingWalk || animationHandler.currentAnimation == RaymanAnimations.LandRun) {
                    animationHandler.SetSpeed(60.0f);
                }
            }

            if ((animationHandler.currentAnimation == RaymanAnimations.RunStop || physicsParams.velocityXZ.magnitude < 0.05f) && InputEx.leftStick.magnitude >= InputEx.deadZone) {
                animationHandler.Set(animationNumber: RaymanAnimations.RunStart, priority: 1.0f);
            } else if (physicsParams.velocityXZ.magnitude > 5 && InputEx.leftStick.magnitude < InputEx.deadZone) {
                animationHandler.Set(animationNumber: RaymanAnimations.RunStop, priority: 1.0f);
            }
            
            if (physicsParams.velocityXZ.magnitude <= 9.0)
            {
                animationHandler.Set(animationNumber: RaymanAnimations.Idle, priority: 9.0f);
            }
            #endregion

            movementStateFlags.helicopter = false;
        }
    }
}
