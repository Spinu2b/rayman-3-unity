﻿using Assets.Scripts.Utils.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.PersoControllers.Rayman
{
    public partial class RaymanPlayerController
    {
        public void RuleSwimming()
        {
            if (rayCollision.atWaterSurface && rayCollision.ground.AnyGround)
            {
                SetRule(RaymanPlayerControllerRules.RuleGroundObj);
                return;
            }

            if (newRule)
            {
                animationHandler.Set(RaymanAnimations.SwimEnter, 1);
                movementStateFlags.helicopter = false;
            }

            animationHandler.SetSpeed(25);
            SetFriction(3, 7);
            moveSpeed = 7;

            rayCollision.ApplyWaterCollision(ref transformParameters.position, ref physicsParams.velocityY);

            if (InputEx.leftStick_s.magnitude > InputEx.deadZone)
            {
                InputMovement();
                RotateToStick(2);
                animationHandler.Set(animationNumber: RaymanAnimations.SwimStartMove, priority: 0);
                if (!animationHandler.IsSet(RaymanAnimations.SwimEnter)) {
                    animationHandler.SetSpeed(InputEx.leftStick_s.magnitude * moveSpeed * 3);
                }
            } else {
                animationHandler.Set(animationNumber: RaymanAnimations.SwimStopMove, priority: 0);
                animationHandler.SetSpeed(22);
            }
        }
    }
}
