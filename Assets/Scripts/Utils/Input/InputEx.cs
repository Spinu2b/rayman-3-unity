﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static UnityEngine.Input;

namespace Assets.Scripts.Utils.Input
{
    [ExecuteAlways]
    public class InputEx : MonoBehaviour
    {
        public static Vector2 leftStick => new Vector2(GetAxisRaw("Horizontal"), GetAxisRaw("Vertical"));
        public static Vector2 rightStick => new Vector2(GetAxisRaw("RHorizontal"), GetAxisRaw("RVertical"));

        public static float deadZone = 0.15f;
        public static float smoothing = 0.05f;

        public static bool leftStickPress => leftStick.magnitude > deadZone;
        public static bool rightStickPress => rightStick.magnitude > deadZone;
        public static float rightStickAngle => Mathf.Atan2(rightStick.x, rightStick.y) * Mathf.Rad2Deg;
        public static Vector2 leftStick_s => _leftStick_s;
        static Vector2 _leftStick_s;
        public static Vector2 rightStick_s => _rightStick_s;
        static Vector2 _rightStick_s;
        public static Vector3 leftStick3D => new Vector3(GetAxisRaw("Horizontal"), 0, GetAxisRaw("Vertical"));
        public static Vector3 rightStick3D => new Vector3(GetAxisRaw("RHorizontal"), 0, GetAxisRaw("RVertical"));
        public static Vector3 leftStick3D_s => new Vector3(leftStick_s.x, 0, leftStick_s.y);
        public static Vector3 rightStick3D_s => new Vector3(rightStick_s.x, 0, rightStick_s.y);

        public static float leftStickAngle => Mathf.Atan2(-leftStick.x, leftStick.y) * Mathf.Rad2Deg;
        public static float leftStickAngleCamera
            => Camera.main.transform.rotation.eulerAngles.y
            + Mathf.Atan2(-leftStick_s.x, -leftStick_s.y) * Mathf.Rad2Deg;
        public static Vector2 leftStickCam_s
            => Matrix4x4.Rotate(Camera.main.transform.rotation).MultiplyVector(leftStick3D_s);

        public static Vector2 mouseDelta;
        static Vector2 mousePositionPrevious;

        public static bool iJumpDown;
        public static bool iJumpHold;
        public static bool iJumpUp;

        public static bool iShootDown;
        public static bool iShootHold;
        public static bool iShootUp;

        public static bool iStrafeDown;
        public static bool iStrafeHold;
        public static bool iStrafeUp;

        void Update()
        {
            float smoothingValue = smoothing == 0 ? 1 : (1f / smoothing) * Time.deltaTime;
            _leftStick_s = Vector3.ClampMagnitude(Vector2.Lerp(_leftStick_s, leftStick, smoothingValue), 1);
            _rightStick_s = Vector3.ClampMagnitude(Vector2.Lerp(_rightStick_s, rightStick, smoothingValue), 1);
            mouseDelta = (Vector2)mousePosition - mousePositionPrevious;
            mousePositionPrevious = mousePosition;

            iJumpDown = GetButtonDown(InputButtons.Jump);
            iJumpHold = GetButton(InputButtons.Jump);
            iJumpUp = GetButtonUp(InputButtons.Jump);

            iShootDown = GetButtonDown(InputButtons.Shoot);
            iShootHold = GetButton(InputButtons.Shoot);
            iShootUp = GetButtonUp(InputButtons.Shoot);

            iStrafeDown = GetButtonDown(InputButtons.Strafe); 
            iStrafeHold = GetButton(InputButtons.Strafe); 
            iStrafeUp = GetButtonUp(InputButtons.Strafe);
        }
    }
}
