﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Utils.Timer
{
    /// <summary>
    /// Convenient class for triggering actions with delays.
    /// </summary>
    public class Timer
    {
        public static List<Timer> timers = new List<Timer>();

        float timeStart, timeEnd;
        public bool active, finished, onFinish;
        public Action onFinishAction;
        public float remaining => timeEnd - Time.fixedTime;
        public float elapsed => Time.fixedTime - timeStart;
        public int frame;

        /// <summary>
        /// Sets Timer.active to True for the duration in seconds. After the duration, finished is set to true, and onFinish is True for one frame.
        /// </summary>
        /// <param name="seconds">Duration in seconds.</param>
        public void Start(float seconds, bool reset = true)
        {
            if (active && !reset) return;
            active = true;
            finished = false;
            frame = 0;
            timeStart = Time.fixedTime;
            timeEnd = timeStart + seconds;
            timers.Add(this);
        }

        /// <summary>
        /// Sets Timer.active to True for the duration in seconds. After the duration, onFinishAction is invoked, finished is set to true, and onFinish is True for one frame.
        /// </summary>
        /// <param name="seconds">Duration in seconds.</param>
        /// <param name="onFinishAction">Invoked after the duration.</param>
        public void Start(float seconds, Action onFinishAction, bool reset = true)
        {
            Start(seconds, reset);
            this.onFinishAction = onFinishAction;
        }

        public void Set(float seconds, bool reset = true) => Start(seconds, reset);
        public void Set(float seconds, Action onFinishAction, bool reset = true) => Start(seconds, onFinishAction, reset);


        public void Abort()
        {
            timers.Remove(this);
            active = false;
        }

        public Timer() => Init(null);
        public Timer(Action onFinishAction) => Init(onFinishAction);
        void Init(Action onFinishAction)
        {
            this.onFinishAction = onFinishAction;
            timers.Add(this);
        }

        /// <summary>
        /// Invokes onFinishAction after a delay in seconds.
        /// </summary>
        /// <param name="seconds">Delay in seconds.</param>
        /// <param name="onFinishAction">Action to invoke after delay.</param>
        /// <returns>Returns the timer created to process the delay.</returns>
        public static Timer StartNew(float seconds, Action onFinishAction)
        {
            var timer = new Timer();
            timer.Start(seconds, onFinishAction);
            return timer;
        }
    }
}
