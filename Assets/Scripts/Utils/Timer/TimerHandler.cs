﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Utils.Timer
{
    public class TimerHandler : MonoBehaviour
    {
        void FixedUpdate()
        {
            for (int timerIndex = 0; timerIndex < Timer.timers.Count; timerIndex++)
            {
                var timer = Timer.timers[timerIndex];

                if (timer.onFinish)
                {
                    timer.onFinish = false;
                    Timer.timers.Remove(timer);
                    continue;
                }
                if (timer.active)
                {
                    timer.frame++;
                    if (timer.remaining <= 0)
                    {
                        timer.active = false;
                        timer.finished = true;
                        timer.onFinish = true;
                        if (timer.onFinishAction != null)
                            timer.onFinishAction.Invoke();
                    }
                }
            }
        }
    }
}
